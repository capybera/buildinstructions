#! /usr/bin/env bash

# Update Ubuntu Installation
#Assume Ubuntu 18.04 LTS
sudo apt-get update && sudo apt-get -y dist-upgrade
#Install node version 14 from vendor repository
#nodejs version in ubuntu 1804 repo is too old for vue.
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs python3-pip
# node version 14.5.0 16/07/2020
# npm version 6.14.6 16/07/2020
# Install yarn from vendor repository
## You may also need development tools to build native addons:
sudo apt-get install -y gcc g++ make
## To install the Yarn package manager, run:
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install -y yarn
# yarn version 1.22.4 16/07/2020
#cd application
#yarn add bootstrap-vue  js-xlsx  @vue/cli vuex
#yarn install

# install sheetjs(https://github.com/SheetJS/sheetjs) using npm
#npm install xlsx

# vuex is required for application-level data sharing
#npm install vuex --save